# NCL

NCL (NCAR Command Language) is an interpreted language designed specifically for
scientific data analysis and visualization. NCAR provides
[official documentation regarding NCL](http://ncl.ucar.edu).

## Usage Summary

### Perlmutter

NCL is avaiable on Perlmutter as part of the [E4S software stack](../e4s/index.md). To use:

```
module load e4s
spack env activate gcc
spack load --only package ncl
ncl mycode.ncl
```

Add `-L/usr/lib64 -lX11 -lcairo -lfreetype` to the link line if needed. Notice each module
or spack load command above can be relatively slow since it is making lots of environment configurations.

## Related Documentation

The following is a list of related Web documentation for NCL:

   * [NCL Tools Documentation](http://www.ncl.ucar.edu/Document/Tools/)
   * [NCL Reference Manual](http://www.ncl.ucar.edu/Document/Manuals/Ref_Manual/)
   * [Getting Started using NCL](http://www.ncl.ucar.edu/Document/Manuals/Getting_Started/)
   * [Category List of NCL Applications](http://www.ncl.ucar.edu/Applications/)
