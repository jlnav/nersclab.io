# Q-Chem

[Q-Chem](http://www.q-chem.com/) is a comprehensive ab initio quantum
chemistry package for accurate predictions of molecular structures,
reactivities, and vibrational, electronic and NMR spectra. The new
release of Q-Chem 6 represents the state-of-the-art of methodology
from the highest performance DFT/HF calculations to high level post-HF
correlation methods:

* Fully integrated graphic interface including molecular builder,
  input generator, contextual help and visualization toolkit (See
  amazing image below generated with IQmol; multiple copies available
  free of charge);
* Dispersion-corrected and double hybrid DFT functionals;
* Faster algorithms for DFT, HF, and coupled-cluster calculations;
* Structures and vibrations of excited states with TD-DFT;
* Methods for mapping complicated potential energy surfaces;
* Efficient valence space models for strong correlation;
* More choices for excited states, solvation, and charge-transfer;
* Effective Fragment Potential and QM/MM for large systems;
* For a complete list of new features, see the
  [Q-Chem update page](https://www.q-chem.com/explore/qc60/).

## Availability and Supported Architectures at NERSC

As of 06/01/2023 Q-Chem is not supported at NERSC. Licensed users are welcome to make their own builds.

## Application Information, Documentation, and Support

[Q-Chem User's manual](https://manual.q-chem.com/latest/)

## Related Applications

* [GAMESS](../gamess/index.md)
* [MOLPRO](../molpro/index.md)
* [NWChem](../nwchem/index.md)

## User Contributed Information

!!! info "Please help us improve this page"
	Users are invited to contribute helpful information and corrections
	through our [GitLab repository](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md).
