# Managing Spin Apps with Helm

## Build Your App, then Clean Up the Generated YAML

With Spin, we recommend creating your application by hand first. Create all the volumes and secrets and make sure your
app works. Next, follow these guidelines for deploying your app with Helm:

- Download the YAML files of the workloads in your app and put them in a `templates` directory.

- Also download the yaml files for every volume and tell Helm to not recreate them on every update by setting:
  `helm.sh/resource-policy: keep`

- Creating .yaml files for secrets is also possible if you have a vault-like store where you keep them

- You can remove most of the kubernetes and rancher annotations with some *notable exceptions*:

  - `field.cattle.io/creatorId`
  - `workload.user.cattle.io/workloadselector`

- You can now pull out any configuration values (for example, env vars, ports, endpoint urls, etc.) and place them in a
  `values.yaml` file.

- Ingresses are a bit of a black box. It seems that most annotations can be safely removed from the ingress definition,
  but the Rancher UI may have trouble displaying the ingress afterwards (the ingress will still work)

  - DNS propagation may take a while so you may want to keep ingresses from being recreated by setting
      `helm.sh/resource-policy: keep` (like for volumes)

  - To connect the ingress to a service,

    - Specify backend service name and port in ingress spec (`spec.rules.http.paths.backend`)

    - Expose the port in the backend service spec (`spec.containers.port`)

    - You do not need any of the rancher-specific ingress annotations

- Some users reported that workloads manually edited by NERSC staff become unmanageable by helm. The only way to recover
  is to perform a helm uninstall and then reinstall.

- If you are missing the following in a workload that is pulling private images from your project registry, you will get
  a permissions error on startup when trying to pull the image:

    ```yaml
    imagePullSecrets:
    - name: registry-nersc
    ```

## Deploying the Result to SPIN

You can deploy a helm chart via:
`helm install --set rancherUserId=$RANCHER_USERID <release-name> <chart  dir>`

or upgrade:
`helm upgrade --set rancherUserId=$RANCHER_USERID iris-test <release-name> <char dir>`

where `release-name` can be anything and `chart dir` is the directory containing your helm chart (eg. ".").

You can either set the default namespace via `kubectl`:
`kubectl config set-context --current --namespace=<your namespace>`

or, specify it via the helm commands, such as:
`helm -n $NAMESPACE -f myvalues.yaml install --set rancherUserId=$RANCHER_USERID <release-name> <char dir>`

where `-n` will set the namespace and `-f` the values file that has your environment-specific values to substitute in
the templates.
